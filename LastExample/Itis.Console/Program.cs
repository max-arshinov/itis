﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Itis.Domain;
using Itis.Domain.Entities;

namespace Itis.Console
{
    class Program
    {
        private static readonly string CategoryName
            = "Моя первая и любмиая категория";

        static void Main(string[] args)
        {
            //using (var dbContext = new ItisDbContext())
            //{
            //    var c = dbContext.Categories.First();
            //    Product product = new Product("Бла-Бла", 10500, c);
            //    dbContext.Products.Add(product);
            //    dbContext.SaveChanges();                
            //}

            using (var dbContext = new ItisDbContext())
            {
                var pr = dbContext.Products
                    .Take(1)
                    .ToList();
            }
        }
    }
}
