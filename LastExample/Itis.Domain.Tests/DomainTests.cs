﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Itis.Domain.Entities;
using Xunit;

namespace Itis.Domain.Tests
{
    public class DomainTests
    {
        private static readonly string CategoryName 
            = "Моя первая и любмиая категория";

        [Fact]
        public void A()
        {
            using (var dbContext = new ItisDbContext())
            {
                dbContext.Categories.Add(new Category(CategoryName));
                dbContext.SaveChanges();
            }

            using (var dbContext = new ItisDbContext())
            {
                var c = dbContext.Categories.ToList();
                Assert.True(c.Any(x => x.Name == CategoryName));
            }
        }

    }
}
