﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using JetBrains.Annotations;

namespace Itis.Domain.Entities
{
    public class Category : HasNameBase
    {
        protected virtual ICollection<Product> ProductsImpl { get; set; }
         = new List<Product>();

        public virtual IEnumerable<Product> Products => ProductsImpl.ToArray();    

        public void AddProduct([NotNull] Product product)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));
            product.Category = this;
            ProductsImpl.Add(product);
        }

        private Category()
        {            
        }

        public Category(string name) : base(name)
        {
        }
    }
}
