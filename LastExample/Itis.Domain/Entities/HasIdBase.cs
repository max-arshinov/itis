﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Itis.Domain
{
    public interface IHasIdBase
    {
        object Id { get; }
    }

    public interface IHasIdBase<out T> : IHasIdBase
    {
        new T Id { get; }
    }

    public abstract class HasIdBase<T> : IHasIdBase<T>
    {
        public T Id { get; set; }

        object IHasIdBase.Id => Id;
    }
}