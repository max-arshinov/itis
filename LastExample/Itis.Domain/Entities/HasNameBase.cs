﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Itis.Domain.Entities
{
    public abstract class HasNameBase : HasIdBase<int>, IHasName
    {
        private string _name;

        protected HasNameBase()
        {
        }

        protected HasNameBase(string name)
        {
            Name = name;
        }

        [Required]
        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(nameof(value));
                }

                _name = value;
            }
        }
    }
}