﻿namespace Itis.Domain.Entities
{
    public interface IHasName
    {
        string Name { get; set; }
    }
}