﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Itis.Domain.Entities
{
    public class Product : HasNameBase, IHasIdBase<int>, IHasName
    {
        protected Product()
        {            
        }

        public Product(string name, decimal price, Category category)
            : base(name)
        {
            Name = name;
            Price = price;
            // ReSharper disable once VirtualMemberCallInConstructor
            Category = category;
        }

        public int Rate { get; set; }

        [Required]
        public virtual Category Category { get; set; }

        private decimal _price;

        [Range(1, int.MaxValue)]
        public decimal Price
        {
            get { return _price; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Value must be > 0",
                        nameof(value));
                }

                _price = value;
            }
        }
    }
}