﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Itis.Domain.Entities;

namespace Itis.Domain
{
    public class ItisDbContext : DbContext
    {
        public ItisDbContext()
            : base("DefaultConnection")
        {
            
        }

        public IDbSet<Product> Products { get; set; }

        public IDbSet<Category> Categories { get; set; }
    }
}
