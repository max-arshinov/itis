namespace Itis.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Product_Add_Rate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Rate", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Rate");
        }
    }
}
