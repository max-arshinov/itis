﻿using System.Linq;
using System.Web.Mvc;
using Itis.Domain;

namespace Itis.Web.Controllers
{
    public class HomeController : Controller
    {
        public string Index()
        {
            using (var dbContext = new ItisDbContext())
            {
                return dbContext.Products.First(x => x.Price > 50)?.Name;
            }
        }
    }
}